package perceptron.util;

import java.io.PrintStream;
import java.util.Random;
import java.util.function.DoubleSupplier;

public class Util {

    public static void fill ( double[][] arr, DoubleSupplier rand) {
        for (int ir = 0; ir < arr.length; ir++) {
            for (int ic = 0; ic < arr[ir].length; ic++) {
                arr[ir][ic] = rand.getAsDouble();
            }
        }
    }
    
    public static void print ( PrintStream out, String title, double [][] arr) {
        out.println("[" + title + "]");
        for (int ir = 0; ir < arr.length; ir++) {
            for (int ic = 0; ic < arr[ir].length; ic++) {
                out.print(String.format("%.5f ", arr[ir][ic]));
            }
            out.println();
        }
        out.println();
    }
    
    private static Random rand = new Random();
    
    public static double random( double min, double max ) {
        rand.setSeed(System.nanoTime());
        return Math.abs( max - min ) * rand.nextDouble() + min;
    }
}
