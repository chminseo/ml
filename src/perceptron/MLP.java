package perceptron;

import java.util.Arrays;

import perceptron.fn.MathFunction;
import perceptron.layer.Layer;

public class MLP {
    
    private static final double BIAS = 1.0;
    private Layer hiddenLayer;
    private Layer outLayer;
    private double learningRate = 1.0;
    private int featureSize ;
    
    private double [] curIn ;
    private double [] curOut ;
    
    public MLP(Layer hiddenLayer, Layer outLayer, double learningRate, int featureSize) {
        super();
        this.hiddenLayer = hiddenLayer;
        this.outLayer = outLayer;
        this.learningRate = learningRate;
        this.featureSize = featureSize;
        
        this.curIn = new double[featureSize + 1];
    }
    

    public double [] training(double [] input) {
        if ( input.length != featureSize ) {
            throw new RuntimeException(String.format("feature size should be %d, but %d", featureSize, input.length) );
        }
        System.arraycopy(input, 0, curIn, 1, input.length);
        curIn[0] = BIAS;
        
        double [] hout = hiddenLayer.process(curIn);
        curOut = outLayer.process(hout);
        
        // strip BIAS 
        double [] out = Arrays.copyOfRange(curOut, 1, curOut.length);
        return out;
    }
    
    public double[] errors(double[] target ) {
        
        /*
         * four arrays contain bias(1.0) at first index
         */
        double [] sumOut = outLayer.getOutSum();
        double [] actOut = outLayer.getActivatedOut();
        double [] sumHdn = hiddenLayer.getOutSum();
        double [] actHdn = hiddenLayer.getActivatedOut();
        
        MathFunction drvOut = outLayer.getActivationFn().derivative();
        double [] dt_out = new double [target.length];
        for ( int i = 0 ; i < dt_out.length; i++ ) {
            dt_out[i] = (target[i] - actOut[i+1]) * drvOut.getOut(sumOut[i+1]);
        }
        
        // weight delta of output layer
        double [][] wdOut = new double[outLayer.size()][ hiddenLayer.size() + 1 ];  
        for ( int k = 0 ; k < wdOut.length; k ++ ) {
            for ( int j = 0 ; j < wdOut[k].length; j ++ ) {
                wdOut[k][j] = learningRate * dt_out[k] * actHdn[j];
            }
        }
        
        MathFunction drvHdn = hiddenLayer.getActivationFn().derivative();
        double [] dt_hdn = new double[ actHdn.length -1 ];
        double [][] WO = outLayer.getWeights();
        for ( int i = 0 ; i < dt_hdn.length; i++ ) {
            double prod_out_layer = 0;
            for ( int k = 0 ; k < WO.length; k ++ ) {
                prod_out_layer += dt_out[k] * WO[k][i+1];
            }
            dt_hdn[i] =  drvHdn.getOut(sumHdn[i + 1]) * prod_out_layer;
        }
        
        // weight delta of hidden layer
        double [][] wdHdn = new double [hiddenLayer.size()][ featureSize + 1];
        for ( int i = 0 ; i < wdHdn.length; i++ ) {
            for ( int j = 0 ; j < wdHdn[i].length; j++) {
                wdHdn[i][j] = learningRate * dt_hdn[i] * curIn[j];
            }
        }
        
        outLayer.updateWeight ( wdOut );
        hiddenLayer.updateWeight(wdHdn);
        return null;
    }


	public double[][] getHiddenWeights() {
		return this.hiddenLayer.getWeights();
	}


	public double[][] getOutWeights() {
		return this.outLayer.getWeights();
	}


	public int getFeatureSize() {
		return this.featureSize;
	}


	public Layer getHiddenLayer() {
		return hiddenLayer;
	}
	
	public Layer getOutLayer() {
		return outLayer;
	}


	public double getLearningRate() {
		return this.learningRate;
	}


	public void setLearningRate(double learingRate) {
		this.learningRate = learingRate;
		
	}
    
    
    
}
