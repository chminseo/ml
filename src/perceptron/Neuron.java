package perceptron;

import perceptron.fn.LinearFn;
import perceptron.fn.MathFunction;

public class Neuron {

    private double weight;
    private double bias;
    
    private MathFunction afn ;
    
    public Neuron(double w, double bias) {
        this ( w, bias, new LinearFn());
    }

    public Neuron(double w, double b, MathFunction afn) {
        this.weight = w;
        this.bias = b;
        this.afn = afn;
    }

    public double feedForward ( double input) {
        final double sigma = weight * input + bias;
        double y = afn.getOut(sigma);
        return y;
    }

    public double weight() {
        return weight;
    }
    
    public double bias() {
        return bias;
    }

    public void update(double wu, double bu) {
        this.weight = wu;
        this.bias = bu;
        
    }
}
