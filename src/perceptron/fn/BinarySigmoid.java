package perceptron.fn;

public class BinarySigmoid implements MathFunction {

    private double slope = 0.5;
    private double dx;

    public BinarySigmoid(){}
    
    public BinarySigmoid(double slope, double dx) {
        this.slope = slope;
        this.dx = dx;
    }

    public BinarySigmoid(double slope) {
        this ( slope, 0.0);
    }

    @Override
    public double getOut(double input) {
        return 1.0 / ( 1 + Math.exp( -1 * slope * (input - dx )) );
    }

    @Override
    public MathFunction derivative() {
        final BinarySigmoid s = this;
        return new MathFunction() {

            @Override
            public double getOut(double input) {
                double o = s.getOut(input);
                return slope * o * (1 - o);
            }

            @Override
            public MathFunction derivative() {
                throw new RuntimeException("not allowed!");
            }
            
        };
    }
    
}