package perceptron.fn;

public class ReLU implements MathFunction {

    @Override
    public double getOut(double input) {
        
        return Math.max(input, 0);
    }

    @Override
    public MathFunction derivative() {
        throw new RuntimeException("ReLU cannot differentative");
    }
    
}