package perceptron.fn;

public interface MathFunction {

    public double getOut ( double input );
    
    public MathFunction derivative();
}
