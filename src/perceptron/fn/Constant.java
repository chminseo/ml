package perceptron.fn;

public class Constant implements MathFunction {

    private double v ;
    public Constant(double v) {
        this.v = v;
    }
    
    @Override
    public double getOut(double x) {
        return v;
    }

    @Override
    public MathFunction derivative() {
        return new Constant(0);
    }

}
