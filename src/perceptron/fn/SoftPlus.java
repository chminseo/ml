package perceptron.fn;

public class SoftPlus implements MathFunction {

	MathFunction d = new DerivatedSoftPlus();
	@Override
	public double getOut(double input) {
		double v = 1 + Math.exp(input);
//		if (v > 0.541255 || Double.isInfinite(v) || Double.isNaN(v)) {
//			v = 0.541255;
//		}
		return Math.log(v);
	}

	@Override
	public MathFunction derivative() {
		return d;
	}
	
	static class DerivatedSoftPlus implements MathFunction {

		@Override
		public double getOut(double input) {
			return 1 / (1 + Math.pow(Math.E, -input));
		}

		@Override
		public MathFunction derivative() {
			throw new RuntimeException("no derivated function");
		}
		
	}

}
