package perceptron.fn;

public class BipolarSigmoid implements MathFunction {

    private double slope = 0.5;
    private MathFunction derivative ;
    public BipolarSigmoid() {}
    
    public BipolarSigmoid(double slope) {
        this.slope = slope;
    }

    @Override
    public double getOut(double input) {
        return 2 / ( 1 + Math.exp(-slope * input )) - 1;
    }

    @Override
    public MathFunction derivative() {
        if ( derivative != null) {
            return derivative;
        }
        BipolarSigmoid f = this;
        derivative = new MathFunction() {
            
            @Override
            public double getOut(double input) {
                double y = f.getOut(input);
                return slope * ( 1 + y ) * (1 - y) * 0.5 ;
            }
            
            @Override
            public MathFunction derivative() {
                throw new RuntimeException("already derivated");
            }
        };
        return derivative;
    }

}
