package perceptron.fn;

public class LinearFn implements MathFunction {

    private double slope = 1.0 ;
    
    public LinearFn() {
        this( 1.0 );
    }
    
    public LinearFn(double slope) {
        this.slope = slope;
    }

    @Override
    public double getOut(double input) {
        return slope * input;
    }

    @Override
    public MathFunction derivative() {
        return new Constant(slope);
    }
}