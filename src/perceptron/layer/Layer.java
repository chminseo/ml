package perceptron.layer;

import perceptron.fn.MathFunction;
import perceptron.util.Util;

public class Layer {

    private double[][] weights ;
    
    private MathFunction activationFn ;
    
    private double [] outsum;
    private double [] activated_out;
    
    public Layer ( int nIn, int nOut, MathFunction activationFn ) {
        this ( new double [nOut][nIn+1], activationFn );
        Util.fill(weights, ()->Util.random(-.5, .5));
    }
    public Layer(double[][] weights, MathFunction activationFn) {
        this.weights = weights;
        this.activationFn = activationFn;
        this.outsum = new double[weights.length + 1] ;
        this.activated_out = new double[weights.length + 1] ;
        this.activated_out[0] = 1.0;
    }

    /**
     * 
     * @param in 이전 레이어의 결과값으로 현재 레이어로 들어온 값.
     * @param activator
     * @return 현재 레이어의 결과값
     */
    public double[] process ( double [] in ) {
        /*
         *    L1             L2
         *    
         *   [ 0 ] -+
         *     .     \
         *     .      \
         *   [ i ] ----+-> [ j ]
         *     .      /
         *     .     /
         *   [ N ] -+
         *     
         */
        int N = in.length;
        int j = 0;
        // outsum[j] = activated_out[j] = 1.0; // bias
        for ( j = 1 ; j < outsum.length ; j++ ) {
            outsum[j] = 0;
            for ( int i = 0 ; i < N ; i++ ) {
                outsum[j] += in[i] * weights[j-1][i];
            }
            activated_out[j] = activationFn.getOut(outsum[j]);
        }
        return activated_out;
    }

    public MathFunction getActivationFn() {
        return this.activationFn;
    }
    /**
     * 이전 레이어와의 affine sum을 반환함.
     * @return
     */
    public double [] getOutSum() {
        return this.outsum;
    }
    /**
     * 이전 레이어와의 affine sum을 activation function에 통과시킨 결과값을 반환함.
     * @return
     */
    public double[] getActivatedOut() {
        return this.activated_out;
    }

    public double[][] getWeights() {
        return this.weights;
    }

    public int size() {
        return outsum.length -1 ;
    }

    public void updateWeight(double[][] delta) {
        checkSize ( "row size does not equals : ", this.weights.length, delta.length);
        for ( int ir = 0 ; ir < delta.length; ir ++ ) {
            checkSize ( String.format("col size does not equals at %d row : ", ir), this.weights[ir].length, delta[ir].length);            
        }
        for (int ir = 0; ir < delta.length; ir++) {
            for (int ic = 0; ic < delta[ir].length; ic++) {
                weights[ir][ic] += delta[ir][ic];
            }
        }
    }

    private void checkSize(String error, int l1, int l2) {
        if ( l1 != l2 ) {
            throw new RuntimeException(error);
        }
        
    }

}
