package perceptron;

import perceptron.fn.BinarySigmoid;
import perceptron.fn.MathFunction;
import perceptron.layer.Layer;

public class MLPBuilder {

	private double learningRate = 0.2;
	private MathFunction activationFn = new BinarySigmoid();
	
	private int featureSize = -1 ;
	private int hiddenLayerSize = -1 ;
	private int outputLayerSize = -1;
	
	private MathFunction activationHd;
	private MathFunction activationOut;
	private double[] wh;
	private double[] wo;
	
	private MLPBuilder () {};
	
	public static MLPBuilder newBuilder () {
		return new MLPBuilder();
	}
	
	public MLPBuilder activator ( MathFunction fn ) {
		this.activationFn = fn;
		return this;
	}
	
	public MLPBuilder feature ( int sz) {
		this.featureSize = sz;
		return this;
	}
	
	public MLPBuilder hiddenLayer ( int sz, MathFunction fnHidden) {
		this.hiddenLayerSize = sz;
		this.activationHd = fnHidden;
		return this;
	}
	
	public MLPBuilder outputLayer ( int sz, MathFunction fnOut) {
		this.outputLayerSize = sz;
		this.activationOut = fnOut;
		return this;
	}
	
	
	public MLPBuilder learingRate ( double rate) {
		this.learningRate = rate;
		return this;
	}
	
	public MLPBuilder setHiddenWeight ( double [] weights ) {
		this.wh = weights;
		return this;
	}
	
	public MLPBuilder setOutputWeight ( double [] weights) {
		this.wo = weights;
		return this;
	}
	
	public MLP make ( ) {
		
		double [][] hiddenWeight = new double [hiddenLayerSize][featureSize+1];
		fill ( wh, hiddenWeight);
		Layer hiddenLayer = new Layer(hiddenWeight, activationHd);
		
		double [][] outputWeight = new double[outputLayerSize][hiddenLayerSize + 1 ];
		fill ( wo, outputWeight);
		Layer outputLayer = new Layer(outputWeight, activationOut);
		
		MLP mlp = new MLP(hiddenLayer, outputLayer, learningRate, featureSize);
		return mlp;
	}

	private void fill(double[] src, double[][] dest) {
		if ( src.length != dest.length * dest[0].length ) {
			throw new RuntimeException("weight length mistmatch");
		}
		int i = 0;
		for ( int ir = 0 ; ir < dest.length; ir ++ ) {
			for ( int ic = 0 ; ic < dest[ir].length; ic ++ ) {
				dest[ir][ic] = src[i];
				i++;
			}
		}
	}
}
