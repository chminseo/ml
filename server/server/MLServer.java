package server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import perceptron.MLP;
import perceptron.MLPBuilder;
import perceptron.fn.BinarySigmoid;
import perceptron.fn.BipolarSigmoid;
import perceptron.fn.MathFunction;
/**
 * -port   12345
 * -hsize  number of nodes in hidden layer
 * -hw     hidden layer weights
 * -ow     output layer wieghts
 * 
 * @author chmin.seo
 *
 */
public class MLServer {
	static boolean running = true;
	static MLP mlp ;
	
	static ExecutorService jobExecutor = Executors.newCachedThreadPool();
	
	public static void main(String[] args) throws FileNotFoundException {
		Param param = parseParam ( args.length == 0 ? "server.properties" : args[0] );
		
		InputStream hw = MLServer.class.getResourceAsStream(param.hiddenWeight());
		InputStream ow = MLServer.class.getResourceAsStream(param.outWeight());
		int nHidden = param.hiddenLayerSize() ;
		mlp = createMLP (hw , ow, nHidden, 0.3 );
		
		
		ServerSocket ssc = null ;
		
		try {
			ssc = new ServerSocket( param.port ( ) );
			System.out.printf("running %s:%d mlp server ...\n" ,ssc.getLocalSocketAddress().toString(), ssc.getLocalPort());
			while ( running ) {
				try {
					Socket client = ssc.accept();
					System.out.println("new req from " + client.getPort() );
					process ( client );
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			closeSocket ( ssc );
		}
		
	}

	private static MLP createMLP(InputStream hwStream, InputStream owStream, int nHiddenLayer, double learingRate) {
		MLPBuilder builder = MLPBuilder.newBuilder();
		MathFunction fnHdn = new BipolarSigmoid(0.2);
		MathFunction fnOut = new BinarySigmoid(0.2);
		mlp = builder.feature(28*28)
				     .hiddenLayer(350,fnHdn)
				     .outputLayer(10, fnOut)
				     .setHiddenWeight(weights(hwStream))
				     .setOutputWeight(weights(owStream))
				     .learingRate(learingRate)
		             .make();
		System.out.println(" * nInput : " + 28*28);
		System.out.println(" * Hidden : " + nHiddenLayer);
		System.out.println(" * nOut   : " + 10);
		return mlp;
		
	}

	private static double[] weights(InputStream in) {
		if ( in == null ) {
			throw new RuntimeException("null weight stream");
		}
		Scanner sc = new Scanner(in);
		List<Double> val = new ArrayList<>();
		while ( sc.hasNext() ) {
			val.add( sc.nextDouble());
		}
		sc.close();
		return val.stream().mapToDouble(v->v.doubleValue()).toArray();
	}
	
	private static void closeSocket(ServerSocket ssc) {
		try {
			ssc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static void process(Socket client) {
		try {
			jobExecutor.execute(new ClientJob(mlp, client));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Param parseParam(String  configFile) throws FileNotFoundException {
		Scanner sc = new Scanner(new File ( configFile ));
		Map<String, Object> data = new HashMap<>();
		
		while ( sc.hasNextLine() ) {
			String [] param = sc.nextLine().split("=");
			data.put(param[0], value(param[1]));	
		}
		sc.close();
		return new Param (data );
	}
	
	private static Object value(String v) {
		try {
			return Integer.parseInt(v);
		} catch (NumberFormatException e) {
			try {
				return Double.parseDouble(v);
			} catch (NumberFormatException e1) {
				return v;
			}
		}
	}

	private static class Param {
		private Map<String, Object> data = new HashMap<>();

		public Param(Map<String, Object> map) {
			this.data = map ;
		}
		
		public int hiddenLayerSize() {
			return ((Integer) data.get("mlp.hsize")).intValue();			
		}

		public String hiddenWeight() {
			return (String) "/" + data.get("mlp.hw.file");
		}
		
		public String outWeight() {
			return (String) "/" + data.get("mlp.ow.file");
		}

		public int port() {
			Integer pnum = (Integer) data.get("server.port");
			return pnum == null ? 34345 : pnum.intValue();
		}
	}
}
