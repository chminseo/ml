package server;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;
import java.util.stream.IntStream;

import javax.net.SocketFactory;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import github.yeori.mnist.util.Util;
import perceptron.MLP;
/**
 * 1. SUBMIT
 *
 *    - ANS :STRING(1)
 *    - IMG :STRING(N)
 *    
 * @author chmin.seo
 *
 */
public class ClientJob implements Runnable {
	private Socket sock ;
	private MLP mlp ;

	private InputStream in ;
	private OutputStream out ;
	public ClientJob(MLP mlp, Socket sock) throws IOException {
		this.mlp = mlp;
		this.sock = sock ;
		in = sock.getInputStream();
		out = sock.getOutputStream();
	}
	
	@Override
	public void run() {
		System.out.println("[CLIENT] starting");
		DataInputStream dis = new DataInputStream(in);
		DataOutputStream dos = new DataOutputStream(out);
		JSONParser parser = new JSONParser();
		try {
			System.out.println("reading json from client... ");
			
			String json = dis.readLine();
			JSONObject req = (JSONObject) parser.parse(json);
			
			String answer = (String) req.get("answer");
			String base64 = (String) req.get("img" );
			System.out.println("[IMG] " + base64);
			byte [] rawbytes = asBytes ( base64.substring(1, base64.length()-1) );
//			byte [] rawbytes = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			if ( rawbytes.length != 28*28 ){
				throw new RuntimeException("invalid image size " + rawbytes.length);
			}
			print(rawbytes);

			double [] input = new double [rawbytes.length];
			conv ( rawbytes, input );
//			print(input);
			double [] output = mlp.training(input);
			
			//mlp.training(input);
			writeResult ( dos, answer, output );
//			dos.writeBytes("{\"sucess\": true }");
//			dos.flush();
			closeConnection();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			System.out.println("goodbye!");
		}
		
	}

	private void writeResult(DataOutputStream dos, String answer, double[] probs) throws IOException {
		int maxIdx = maxArgs ( probs );
		System.out.printf("ANSWER:%s, GUESS:%d\n", answer, maxIdx);
		for (int i = 0; i < probs.length; i++) {
			System.out.println(String.format("[%d] %.9f", i, probs[i]));
		}
		JSONObject json = new JSONObject();
		json.put("answer", answer);
		json.put("probs", Arrays.asList(Arrays.stream(probs).boxed().toArray(L->new Double[L])) );
		json.put("guessed", "" + maxIdx );
		String r = json.toJSONString();
		dos.writeUTF(r);
		dos.flush();
	}

	private int maxArgs(double[] probs) {
		return IntStream.range(0, probs.length)
				        .reduce((i, j) -> probs[i] >= probs[j] ? i : j )
				        .getAsInt();
	}

	private void print(byte[] img) {
		/* for display */
		for ( int ir = 0; ir < 28 ; ir ++) {
			for ( int ic = 0 ; ic < 28 ; ic ++ ) {
				System.out.print( String.format("%3s", Integer.toHexString(Util.b2i(img[28*ir + ic])).toUpperCase() ) );
			}
			System.out.println();
		}
		
	}
	private void print(double[] img) {
		/* for display */
		for ( int ir = 0; ir < 28 ; ir ++) {
			for ( int ic = 0 ; ic < 28 ; ic ++ ) {
				System.out.print( String.format("%5.2f", img[28*ir + ic] ) );
			}
			System.out.println();
		}
		
	}

	private byte[] asBytes(String byteStr) {
		String [] nums = byteStr.split(",");
		byte [] data = new byte[nums.length];
		for (int i = 0; i < data.length; i++) {
			data[i] = (byte) (255 - Integer.parseInt(nums[i]));
		}
		
		return data;
	}

	private byte[] loadPNG(File png) throws FileNotFoundException, IOException {
		try ( FileInputStream fos = new FileInputStream(png) ) {
			byte [] data = new byte [ (int) png.length()];
			fos.read(data, 0, data.length);
			return data;
		} 
	}

	private void writeImg(byte[] bytes, String fname) {
		File f = new File(fname);
		try( FileOutputStream fos = new FileOutputStream(f)) {
			fos.write(bytes, 0, bytes.length);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private double[] conv( byte[] from , double[] to) {
		for (int i = 0; i < to.length; i++) {
			to[i] = (Util.b2i(from[i]) - 128) / 128.0;
		}
		return to;
	}

	private void closeConnection() {
		try {
			sock.shutdownOutput();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			sock.shutdownInput();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
