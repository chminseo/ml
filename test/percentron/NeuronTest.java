package percentron;

import static org.junit.Assert.*;

import org.junit.Test;

import perceptron.Neuron;
import perceptron.fn.LinearFn;
import perceptron.fn.ReLU;

public class NeuronTest {
    final static int X = 0, Y = 1;
    @Test
    public void test() {
        
        Neuron nrn = new Neuron(Math.random(), Math.random(), new ReLU());
        UpdateFn fn = new UpdateFn() ;
        
        double [] testData = { 1.0, 4.0 }; //1.0에 대해서 4.0이 나와야 함.
        int trial = 0;
        
        double threshold = 0.00000000000001 ;
        double error = Double.MAX_VALUE;
        while ( Math.abs(error) > threshold) {
            double y = nrn.feedForward(testData[X]);
            error = 0.5 * ( testData[X] - y ) * ( testData[Y] - y );
            fn.updateNeuron(nrn, testData, y);
            System.out.println((++trial) + ": " + error);
        }
        
        System.out.println(String.format("W: %f, b: %f => estimated : %f", nrn.weight(), nrn.bias(), nrn.feedForward(testData[0])));
    }
    
    static class UpdateFn {
        double learningRate = 0.33;
        public void updateNeuron ( Neuron nrn, double[] expected, double y) {
            /* update neuron
             * W` = W - alpha * (Yt - Y ) * x 
             * B` = B - alpha * (Yt - Y ) * 1
             */
            double Wu = nrn.weight() - learningRate * ( y - expected[Y]) * expected[X] ;
            double Bu = nrn.bias()   - learningRate * ( y - expected[Y]);
            nrn.update ( Wu, Bu);
            
        }
    }

}
