package percentron;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import perceptron.MLP;
import perceptron.fn.BinarySigmoid;
import perceptron.fn.MathFunction;
import perceptron.layer.Layer;
import perceptron.util.Util;

public class PhysicalTest {

    @Test
    public void test() {

        double[][] test = { 
                { 165, 50, 0,1}, 
                { 177, 70, 1,0}, 
                { 163, 45, 0,1}, 
                { 179, 68, 1,0},
                
                { 160, 56,0,1}, 
                { 170, 69, 1,0}, 
                { 170, 61,0,1}, 
                { 180, 72, 1,0}, 
                { 161, 65,0,1},
                { 175, 79, 1,0}, 
                { 162, 50,0,1}, 
                { 176, 58, 1,0},
                { 167, 62,0,1}, 
                { 182, 69, 1,0}, 
                { 168, 51,0,1}, 
                { 178, 85, 1,0}, 
                { 153, 47,0,1}, 
                { 188, 80, 1,0}, 
                { 155, 50,0,1}, 
                { 168, 55, 1,0}, 
                { 169, 50,0,1},
                { 174, 63, 1,0}, 

                { 171, 73, 1,0} 
        };
        double [] avrWH = {0,0};
        avrWH[0] = Arrays.stream(test).mapToDouble(e-> e[0]).average().getAsDouble();
        avrWH[1] = Arrays.stream(test).mapToDouble(e-> e[1]).average().getAsDouble();

        int featureSize = 2;
        int hiddenSize = 2;
        int outputSize = 2;
        MathFunction fnHidden = new BinarySigmoid(1.0);
        MathFunction fnOut = new BinarySigmoid(1.0);
        
        double [][] wh = {
                { -0.01, 0.01, 0.03},
                { 0.023, -0.15, -0.01}
        };
        double [][] wo = {
                { 0.03, 0.025, 0.045},
                { -0.023, 0.033, -0.01}
        };
        Layer hidden = new Layer(wh, fnHidden);
        Layer out = new Layer( wo, fnOut);
        
        MLP mp = new MLP(hidden, out, 1.0, featureSize);
        
        Util.print(System.out, "W:hidden", hidden.getWeights());
        Util.print(System.out, "W:out", out.getWeights());
        
        for ( int i = 0  ; i < 90 ; i++ ) {
            for ( int k = 0 ; k < test.length ; k ++) {
                double [] in = Arrays.copyOfRange(test[k], 0, 2) ;
                double [] t = Arrays.copyOfRange(test[k], 2, 4);
                in = normalizeWH ( in, avrWH);
                
                double [] o = mp.training( in );
                mp.errors( t );
                System.out.println(Arrays.toString(in) + ", err: " + error(t, o));
            }
        }
        Util.print(System.out, "W:hidden", hidden.getWeights());
        Util.print(System.out, "W:out", out.getWeights());
        
        double totalError = 0;
        
        double [] r = mp.training(normalizeWH ( new double[]{163, 46}, avrWH));
        System.out.println("F: " + Arrays.toString(r) + ", error: " + error(new double[]{0.0,  1.0}, r) );
        totalError += error(new double[]{0.0,  1.0}, r);
        
        r = mp.training(normalizeWH(new double[]{178, 75}, avrWH));
        System.out.println("M: " + Arrays.toString(r) + ", error: " + error(new double[]{1.0,  0.0}, r)  );
        totalError += error(new double[]{1.0,  0.0}, r);
        
        r = mp.training(normalizeWH(new double[]{168,  51}, avrWH));
        System.out.println("F: " + Arrays.toString(r) + ", error: " + error(new double[]{0.0,  1.0}, r) );
        totalError += error(new double[]{0.0,  1.0}, r);
        
        r = mp.training(normalizeWH(new double[]{181,  85}, avrWH));
        System.out.println("M: " + Arrays.toString(r) + ", error: " + error(new double[]{1.0,  0.0}, r)  );
        totalError += error(new double[]{1.0,  0.0}, r);
        
        System.out.println("Total Error: " + totalError);

    }
    
    private double [] normalizeWH(double[] wh, double[] avr) {
        wh[0] = 6*(wh[0] - avr[0])/avr[0];
        wh[1] = 6*(wh[1] - avr[1])/avr[1];
        return wh;
    }

    double error ( double [] t, double[] o) {
        double sum = 0;
        for (int i = 0; i < o.length; i++) {
            sum += (t[i] - o[i]) * (t[i] - o[i]);
        }
        return sum * 0.5;
    }

}
