package percentron;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import perceptron.util.Util;

public class TestUtil {

    @Test
    public void test() {
        List<Double> oob = new ArrayList<>();
        
        for ( int i = 0 ; i < 1_000_000 ; i++ ) {
            double r = Util.random(5, 20);
            if ( r <= 5 || r>= 20 ){
                oob.add(r);
            }
        }
        System.out.println(oob);
        
        oob.clear();
        
        for (int i = 0; i < 1000; i++) {
            System.out.println(Util.random(-.5, .5));
        }
    }

}
