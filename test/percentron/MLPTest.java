package percentron;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import perceptron.MLP;
import perceptron.fn.BinarySigmoid;
import perceptron.fn.BipolarSigmoid;
import perceptron.fn.MathFunction;
import perceptron.layer.Layer;
import perceptron.util.Util;

public class MLPTest {

    static double BIAS = 1.0;
    @Test
    public void test() {
//        MathFunction sigmoid = new BipolarSigmoid(1.0);
        MathFunction sigmoid = new BinarySigmoid(1.0);
        
        double [][] hw = {
            { 0.3,  0.4,  0.2},
            {-0.1, -0.5,  0.1}
        };
        Layer h = new Layer(hw, sigmoid);
        
        double [][] ow = {
            { 0.1, -0.2,  0.4},
            { 0.2,  0.3, -0.1}
        };
        Layer o = new Layer(ow, sigmoid);
        
        final double learingRate = 1.0;
        int featureSize = 2;
        MLP mp = new MLP(h, o, learingRate, featureSize);
        
        double [][] test = {{12.5, 1.3}};
        double [][] expected = {{0.5, 0.5}};
        double error = Double.MAX_VALUE ;
        double threshold = 0.001;
        int trial = 1;
//        Util.print(System.out, "WEIGHT(HIDDEN)", hw);
//        Util.print(System.out, "WEIGHT(OUTPUT)", ow);
        while ( error > threshold ) {
            double[] output = mp.training (test[0]);
            error = error ( expected[0], output);
            System.out.println(String.format("%3d] %.12f,%.12f, error: %.12f", trial++, output[0], output[1], error ) );
            mp.errors (expected[0]);
        }
        
        Util.print(System.out, "WEIGHT(HIDDEN)", hw);
        Util.print(System.out, "WEIGHT(OUTPUT)", ow);
        
        double [] r = mp.training(new double[]{12.0, 33.0 });
        System.out.println( Arrays.toString(r));
    }
    
    double error ( double [] t, double[] o) {
        double sum = 0;
        for (int i = 0; i < o.length; i++) {
            sum += (t[i] - o[i]) * (t[i] - o[i]);
        }
        return sum * 0.5;
    }

}
