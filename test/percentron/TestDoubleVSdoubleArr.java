package percentron;

import static org.junit.Assert.*;

import java.security.SecureRandom;
import java.util.Random;

import org.junit.Test;

public class TestDoubleVSdoubleArr {

    public static void main(String[] args) {
        test();
    }
    public static void test() {
        Random rand = new Random();
        final int size = 1_000_0000;
        final int loop = 100;
        long total = 0;
        
        long t1;
        double [][] arr = new double[size][];
        for ( int i = 0 ; i < loop ; i++ ) {
            t1 = generateRawArray(arr, arr.length);
            total += t1;
            System.out.println("RAW ARRAY     : " + t1 + " milliseconds");            
        }
        
        System.out.println("AVR           : " + ( total / (1.0*loop)) );
        

        long t2 = 0;
        total = 0;
        Double[] wrapped= new Double[size];
        for ( int i = 0 ; i < loop ; i++ ) {
            t2 = generateWrappedArray(wrapped, wrapped.length);
            total += t2;
            System.out.println("WRAPPED ARRAY : " + t2 + " milliseconds");
            
        }
        System.out.println("AVR           : " + ( total / (1.0*loop)) );

        
        long t3 = 0;
        total = 0;
        double [] arr1 = new double[size];
        for ( int i = 0 ; i < loop ; i++ ) {
            t3 = generateArr(arr1, arr1.length);
            total += t3;
            System.out.println("WRAPPED ARRAY : " + t3 + " milliseconds");
        }
        System.out.println("AVR           : " + ( total / (1.0*loop)) );
       
    }
    
    public static long generateRawArray (double [][] input, int row) {        
//        Random rand = new SecureRandom(); 
        long s = System.currentTimeMillis();
        
        for( int i = 0 ; i < row ; i++) {
            input[i] = new double[]{ 0.23232233 };
        }
        
        return System.currentTimeMillis() -s ;
    }
    
    public static long generateWrappedArray (Double [] input, int row) {
//        Random rand = new SecureRandom(); 
        long s = System.currentTimeMillis();
        
        for( int i = 0 ; i < row ; i++) {
            input[i] = 0.23232233; //rand.nextDouble();
        }
        
        return System.currentTimeMillis() - s ;
    }
    
    public static long generateArr ( double [] input, int row) {
        long s = System.currentTimeMillis();
        
        for( int i = 0 ; i < row ; i++) {
            input[i] = 0.23232233; //rand.nextDouble();
        }
        
        return System.currentTimeMillis() - s ;
        
    }

}
