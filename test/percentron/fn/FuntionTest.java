package percentron.fn;

import static org.junit.Assert.*;

import org.junit.Test;

import perceptron.fn.BinarySigmoid;
import perceptron.fn.BipolarSigmoid;
import perceptron.fn.MathFunction;
import perceptron.fn.SoftPlus;

public class FuntionTest {

    @Test
    public void test_sigmoid() {
        BinarySigmoid f = new BinarySigmoid(1.0);
        assertEquals ( 0.5, f.getOut(0), 0.1111111);
        assertEquals ( 0.269, f.getOut(-1.0), 0.111);
        assertEquals ( 0.731, f.getOut( 1.0), 0.111);
        
        MathFunction df = f.derivative();
        assertEquals ( 0.19661193, df.getOut(-1), 0.11111111);
        assertEquals ( 0.25,       df.getOut(0), 0.111);
        assertEquals ( 0.19661193, df.getOut(+1), 0.11111111);
    }
    
    @Test
    public void test_bipolar_sigmoid() {
        BipolarSigmoid sgm = new BipolarSigmoid();
        System.out.println(sgm.getOut(-0.04479));
        
        MathFunction df = sgm.derivative();
        System.out.println( df.getOut(-0.04479));
    }
    
    @Test
    public void test_softplus() {
    	SoftPlus fn = new SoftPlus();
    	assertEquals(1.313, fn.getOut(1.0), 0.111);
    	assertEquals(1.0, fn.getOut(0.541255), 0.1);
    	
    	assertEquals(0.5, fn.derivative().getOut(0.0), 0.5);
    	
    }

}
