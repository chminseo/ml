package reporting;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class ErrorReporter {
	private static String [] resultsFiles ;
	public static void main(String[] args) throws FileNotFoundException {
		resultsFiles = pickResults ( ); // new String[]{"result/L350-0.20rate-30epochs-sigmoid-20160930_153237.txt"};
//		Arrays.stream(resultsFiles).forEach(System.out::println);
		for ( int i = resultsFiles.length-1 ; i >= 0 ; i-- ) {
			String fname = resultsFiles[i];
			String csvpath = asCsvFileName ("result/csv", fname );
//			System.out.println("exporting " + fname + " to " + csvpath);
			int [][] errmap = createErrorMap ( fname );
//			transpose ( errmap);
			write ( errmap, csvpath );
		}
	}
	private static void transpose(int[][] map) {
		int tmp = 0;
		for (int ir = 0; ir < map.length; ir++) {
			for (int ic = 0; ic < ir; ic++) {
				tmp = map[ir][ic];
				map[ir][ic] = map[ic][ir];
				map[ic][ir] = tmp;
				
			}
		}
	}
	private static String[] pickResults() {
		final String pattern = "^L.+txt$";
		File [] resultsFiles = new File("result").listFiles(new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				String name = pathname.getName();
				return name.matches(pattern);
			}
		});
		return Arrays.stream(resultsFiles)
				     .map(f-> "result/" + f.getName())
				     .toArray(len-> new String[len]);
	}
	private static void write(int[][] map, String path) throws FileNotFoundException {
		File outfile = new File(path);
		try ( PrintStream out = new PrintStream(new FileOutputStream(outfile) )){
			
			// header
			out.printf("###,[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],SUM(ANS)\n");
			int totalError = 0;
			for (int ir = 0; ir < map.length; ir++) {
				out.printf("[%d]", ir);
				int rowsum = 0;
				for (int ic = 0; ic < map[ir].length; ic++) {
					out.printf(",%d", map[ir][ic]);
					rowsum += map[ir][ic];
				}
				totalError += rowsum;
				out.printf(",%d\n", rowsum);
			}
			
			// sum of columns
			out.print("SUM(OUT)");
			System.out.printf("L%d-%d", 
					Integer.parseInt(path.substring("result/csv/".length() + 1,"result/csv/".length() + 4)), 
					Integer.parseInt(path.substring(path.indexOf("epochs")-2,path.indexOf("epochs"))));
			for (int ic = 0; ic < map.length; ic++) {
				int _ic = ic;
				int sumOfCol = IntStream.range(0, 10).map(ir-> map[ir][_ic]).sum();
				out.print("," + sumOfCol );
				System.out.printf(",%d", sumOfCol);
			}
			out.printf(",%d\n", totalError);
			System.out.printf(",%d\n", totalError);
			out.flush();
		}
		
	}
	private static int [][] createErrorMap(String input) throws FileNotFoundException {
		int [][] errormap = new int [10][10];
		
		try ( Scanner sc = new Scanner(new File( input) )) {
			while ( sc.hasNextLine()) {
				String line = sc.nextLine().trim();
				if ( line.startsWith("[")) {
					recordError ( errormap, line );
				}
				
			}
		}
		return errormap;
		
		
	}
	private static void recordError(int[][] map, String line) {
		//[   8] target : '5', result: '6'
		int mnistIndex = parseMnistIndex ( line );
		int answerNum = parseAnswer( line );
		int outputNum = parseOutput( line );
		// 정답 answer 를 output 으로 잘못 인식함.
		map[answerNum][outputNum] ++ ;
		
	}
	
	private static int parseOutput(String line) {
		int offset = line.indexOf("result");
		int p0 = line.indexOf("'", offset) + 1;
		int p1 = line.indexOf("'", p0);
		
		return Integer.parseInt(line.substring(p0, p1).trim());
	}
	private static int parseAnswer(String line) {
		int p0 = line.indexOf("'", 0) + 1;
		int p1 = line.indexOf("'", p0 );
		
		return Integer.parseInt(line.substring(p0, p1).trim());
	}
	private static int parseMnistIndex(String line) {
		int p0 = line.indexOf('[') + 1;
		if ( p0 <= 0 ) {
			throw new RuntimeException("'['가 없음");
		}
		
		int p1 = line.indexOf(']', p0);
		if ( p1 < 0 ) {
			throw new RuntimeException("']'가 없음");
		}
		return Integer.parseInt(line.substring(p0, p1).trim());
	}
	private static String asCsvFileName(String outdir, String fname) {
		int p0 = fname.lastIndexOf('/');
		int p1 = fname.lastIndexOf('.');
		
		String path = fname.substring(p0 + 1, p1) + ".csv";
		outdir = outdir.endsWith("/") ? outdir : outdir + "/";
		return outdir + path ;
	}
}
