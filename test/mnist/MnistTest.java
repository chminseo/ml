package mnist;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Ignore;
import org.junit.Test;

import github.yeori.mnist.MnistUtil;
import github.yeori.mnist.db.MnistDb;
import github.yeori.mnist.db.MnistLoop;
import github.yeori.mnist.db.Mnistlet;
import github.yeori.mnist.util.Util;
import perceptron.MLP;
import perceptron.MLPBuilder;
import perceptron.fn.BinarySigmoid;
import perceptron.fn.BipolarSigmoid;
import perceptron.fn.MathFunction;
import perceptron.fn.SoftPlus;
import perceptron.layer.Layer;

public class MnistTest {

	int featureSize = 28*28 ; //pixels in a image
	int hiddenSize = 100;// 200;
	int outputSize = 10; // 0 ~ 9
	
	double learingRate = 1.0;
	
	int epochs = 100; //50;
	DateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
	
	@Ignore @Test
	public void wgen() {
		Date cur = new Date();
		Random r = new SecureRandom();
		int HN = hiddenSize * (featureSize + 1);
		int ON = outputSize * (hiddenSize + 1);
		double [] weight = new double[HN];
		double range = 0;
		for ( int i = 0 ; i < HN ; i++ ) {
//			weight[i] = (r.nextDouble()-0.5)*0.8 ;
			range = Math.sqrt(6.0/(outputSize + featureSize)); 
			weight[i] = (r.nextDouble() * 2 * range ) - range ;
		}
		write(new double[][]{weight}, String.format("test/mnist/hidden-XV%d.weight", hiddenSize));
		
		weight = new double[ON];
		for ( int i = 0 ; i < ON ; i++ ) {
			weight[i] = (r.nextDouble()-0.5)*0.8 ;
		}
		write(new double[][]{weight}, String.format("test/mnist/out-XV%d.weight", hiddenSize));
	}
	@Test
	public void test() throws FileNotFoundException {
		MnistDb training = MnistUtil.loadDb("e:/mnist/train-labels.idx1-ubyte", "e:/mnist/train-images.idx3-ubyte");
		MnistDb testDb = MnistUtil.loadDb("e:/mnist/t10k-labels.idx1-ubyte", "e:/mnist/t10k-images.idx3-ubyte");
		
		MLPBuilder builder = MLPBuilder.newBuilder();
		MathFunction fnHdn = new BipolarSigmoid(0.2);
		MathFunction fnOut = new BinarySigmoid(0.2);
		MLP mlp = builder.activator(fnOut)
		       .feature(featureSize)
		       .hiddenLayer(hiddenSize, fnHdn)
		       .outputLayer(outputSize, fnOut)
		       .learingRate(learingRate)
		       .setHiddenWeight(weights(String.format("hidden-XV%d.weight", hiddenSize)))
		       .setOutputWeight(weights(String.format("out-XV%d.weight",    hiddenSize)))
		       .make();
		
		double [] input = new double[featureSize];
		double [] target = new double[outputSize];
		Mnistlet mlet = null;
		
		DateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date cur = new Date();
		Error err = new Error(training.size());

		int actual, expected ;
		List<Integer> idx = IntStream.range(0, training.size()).boxed().collect(Collectors.toList());
		for ( int step = 0 ; step < epochs ; step ++ ) {
			
			if ( step > 0 && step % 5 == 0 ){
				createTestingReport (testDb, mlp, step);
			}
			
			// y = 0.0002*(x-50)^2 + 0.1
			mlp.setLearningRate(nextlearningRate5(step));
			
			double e ;
			err.reset();
			/*
			int offset = 0;
			if (step % 2 == 0) {
				Collections.shuffle(idx);
				offset = 0;
			} else {
				offset = idx.size()/2;
			}
			*/
			Collections.shuffle(idx);
			for ( int i = 0 ; i < 40000; i++ ) { // idx.size()
				// mlet = training.get( idx.get(i+offset) );
				mlet = training.get( idx.get(i) );
				conv( mlet.rawbytes(), input );
				double[] output = mlp.training(input);
				number ( target, mlet.number() );
				// 틀린 갯수 기록
				actual = maxarg(output);
				expected = mlet.number() - '0';
				if ( expected != actual ) {
					err.missed ();
					mlp.errors(target) ;
				}
				// error rate 기록 
				e = error ( target, output);
				err.put(e);
//				if ( i % 10000 ==0 ) {
//					System.out.printf("STEP: %d, ERROR: %f, learingRate: %.3f\n", step, e, mlp.getLearningRate());
//				}
			}
			System.out.printf("STEP %d LearingRate: %.3f, RESULT: %s\n\n",step, mlp.getLearningRate(), err.toString());
//			log.printf(       "STEP %d RESULT: %s\n", step, err.toString());
			
		}
//		if ( step > 0 && step % 5 == 0 ){
		createTestingReport (testDb, mlp, epochs);
//		}
	}
	
	private double calcuateLearningRate(int step) {
		// y = 0.0002*(x-50)^2 + 0.1
		// [0.6 ~ 0.1] for 50 epoches
		// ‭1.9387755102040816326530612244898e-4‬
		double base = 0.9/(epochs*epochs);
		return base*(step - epochs)*(step - epochs) + 0.1;
	}
	
	private double nextlearningRate3(int step) {
		// y = 0.00038*(x-50)^2 + 0.05
		// [1.0 ~ 0.05] for epoches
		double base = 0.95/(epochs*epochs);
		return base*(step - epochs)*(step - epochs) + 0.05;
	}
	
	private double nextlearningRate5(int step) {
		// y = 0.00038*(x-50)^2 + 0.05
		// [1.0 ~ 0.05] for epoches
		/*
		 * -0.95
		 * ----- * X^2 + 1
		 *   100 
		 */
		double base = -0.95/(epochs*epochs);
		return step*step*base + 1;
	}
	private double nextlearningRate4(int step) {
		// y = 0.00038*(x-50)^2 + 0.05
		// [1.0 ~ 0.05] for epoches
		if (step < 20) {
			return 1;
		} else if (step < 30) {
			return 0.9;
		}
		double base = 0.95/(epochs*epochs);
		return base*(step - epochs)*(step - epochs) + 0.05;
	}
	
	private double nextLearningRate(int step) {
		/*
		 *            1
		 * f(x) = ----------,  [(0, 0.6) ~ (50, 0.019)]
		 *        x + 1.6667
		 */
		return 1 / (step + 1.6667);
	}

	private void createTestingReport(MnistDb db, MLP mlp, int epochs ) throws FileNotFoundException {
		Date cur = new Date();
		PrintStream log = new PrintStream(String.format(
				"result/L%03d-%.2frate-%02depochs-sigmoid-idx.shuffle-bps.2-bns.2-wXV-%s.txt", 
//				"result/L%03d-%.2frate-%02depochs-sigmoid-idx.shuffle-bns.2-bns.2-wXV-%s.txt", 
				mlp.getHiddenLayer().size(), learingRate, epochs, df.format(cur) ));
		
		double [][] hw = mlp.getHiddenWeights();
		double [][] ow = mlp.getOutWeights();
		
		write( hw, String.format("weights/%s-hw-L%03d-%.2frate-%depoch-idx.shuffle-bps.2-bns.2-wXV.weights", df.format(cur), hiddenSize, learingRate, epochs));
		write( ow, String.format("weights/%s-ow-L%03d-%.2frate-%depoch-idx.shuffle-bps.2-bns.2-wXV.weights", df.format(cur), hiddenSize, learingRate, epochs));
//		write( hw, String.format("weights/%s-hw-L%03d-%.2frate-%depoch-idx.shuffle-softplus-softplus-wXV.weights", df.format(cur), hiddenSize, learingRate, epochs));
//		write( ow, String.format("weights/%s-ow-L%03d-%.2frate-%depoch-idx.shuffle-softplus-softplus-wXV.weights", df.format(cur), hiddenSize, learingRate, epochs));
		
		MnistLoop loop = db.iterator();
		int nSuccess = 0;
		int nFail = 0;
		Mnistlet mlet = null;
		double [] input = new double[mlp.getFeatureSize()];
		StringBuilder falseNums = new StringBuilder();
		for ( int i = 0 ; i < loop.size() ; i ++ ) {
			mlet = loop.get(i);
			char tn = mlet.number();
			conv(mlet.rawbytes(), input);
			double [] result = mlp.training(input);
			char guess = (char) ('0' + maxarg ( result ));
			if( tn == guess ) {
				nSuccess ++ ;
			} else {
				nFail ++ ;
				falseNums.append( String.format("[%4d] target : '%s', result: '%s'\n", i, mlet.number(), guess) );
			}
			
		}
		log.printf("# hidden      : %d\n", mlp.getHiddenLayer().size());
		log.printf("# learing rate: %f\n", mlp.getLearningRate());
		log.printf("# epochs      : %d\n", epochs);
		log.printf("\n");
		log.println("# total   : " + (nSuccess + nFail));
		log.println("# success : " + nSuccess);
		log.println("# fail    : " + nFail);
		log.printf( "# error   : %f\n\n", 100.0*nFail / (nSuccess + nFail));
		
		System.out.println("# total   : " + (nSuccess + nFail));
		System.out.println("# success : " + nSuccess);
		System.out.println("# fail    : " + nFail);
		System.out.printf( "# error   : %f\n", 100.0*nFail / (nSuccess + nFail));
		
		
		log.print( falseNums.toString() );
		log.close();
	}
	private double[] weights(String wfile) {
		InputStream in = MnistTest.class.getResourceAsStream(wfile);
		if ( in == null ) {
			throw new RuntimeException("no such file: " + wfile);
		}
		Scanner sc = new Scanner(in);
		List<Double> val = new ArrayList<>();
		
		while ( sc.hasNext()) {
			val.add(sc.nextDouble());
		}
		return val.stream().mapToDouble(v->v.doubleValue()).toArray();
	}
	
	private void write( double[][] w, String fname) {
		File f = new File(fname);
		try(FileOutputStream fos = new FileOutputStream(f);
			PrintStream out = new PrintStream(fos)) {
			f.createNewFile();
			for (int ir = 0; ir < w.length; ir++) {
				for (int ic = 0; ic < w[ir].length; ic++) {
					out.printf(String.format("%.8f\n", w[ir][ic]) );
				}
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	/**
	 * 가장 높은 weight를 갖는 index 반환 
	 * @param result
	 * @return
	 */
	private int maxarg(double[] result) {
		int idx = 0;
		double max = result[idx];
		for (int i = 0; i < result.length; i++) {
			if ( result[i] > max ) {
				idx = i;
				max = result[i];
			}
		}
		return idx;
	}
	double error ( double [] t, double[] o) {
        double sum = 0;
        for (int i = 0; i < o.length; i++) {
            sum += (t[i] - o[i]) * (t[i] - o[i]);
        }
        return sum * 0.5;
    }
	
	private void number(double[] target, char num) {
		for (int i = 0; i < target.length; i++) {
			target[i] = 0;
		}
		target[ num - '0' ] = 1;
	}
	private void fillWeight(double[][] w, String wfile) {
		InputStream in = MnistTest.class.getResourceAsStream(wfile);
		if ( in == null ) {
			throw new RuntimeException("no such file: " + wfile);
		}
		Scanner sc = new Scanner(in);
		
		for ( int ir = 0 ; ir < w.length ; ir ++ ) {
			for ( int ic = 0 ; ic < w[ir].length; ic ++) {
				w[ir][ic] = sc.nextDouble();
			}
		}
	}
	private double[] conv( byte[] from , double[] to) {
		for (int i = 0; i < to.length; i++) {
			to[i] = (Util.b2i(from[i]) - 128) / 128.0;
//			to[i] = Util.b2i(from[i]) / 255.0;
		}
		return to;
	}

}
