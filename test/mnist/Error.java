package mnist;

import java.util.Arrays;

public class Error {

	double [] elems;
	double sum;
	int i ;
	private int nMissed;
	public Error ( int sz ) {
		elems = new double [sz];
	}
	
	public void put ( double d) {
		elems[i++] = d;
		sum += d;
	}
	
	public void reset() {
		Arrays.fill(elems, 0);
		sum = i = nMissed = 0;
	}
	
	@Override
	public String toString() {
		double avr = sum / elems.length;
		double stdv = Arrays.stream(elems).map(d-> (d-avr)*(d-avr)).average().getAsDouble();
		return String.format("FAIL: %3d cases, AVR: %f, STDV: %f",nMissed, avr, stdv);
	}

	public void missed() {
		nMissed ++;
	}
}
