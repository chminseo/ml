package mnist;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

import org.junit.Test;

public class WeightGenerator {

	@Test
	public void generateWeights() {
		int featureSize = 28*28 ; //pixels in a image
		int hiddenSize = 50;// 200;
		int outputSize = 10; // 0 ~ 9
		
		
		Date cur = new Date();
		Random r = new SecureRandom();
		int HN = hiddenSize * (featureSize + 1);
		int ON = outputSize * (hiddenSize + 1);
		double [] weight = new double[HN];
		double range = 0;
		for ( int i = 0 ; i < HN ; i++ ) {
//			weight[i] = (r.nextDouble()-0.5)*0.8 ;
			range = Math.sqrt(6.0/(outputSize + featureSize)); 
			weight[i] = (r.nextDouble() * 2 * range ) - range ;
		}
		write(new double[][]{weight}, String.format("test/mnist/hidden-XV%d.weight", hiddenSize));
		
		weight = new double[ON];
		for ( int i = 0 ; i < ON ; i++ ) {
			weight[i] = (r.nextDouble()-0.5)*0.8 ;
		}
		write(new double[][]{weight}, String.format("test/mnist/out-XV%d.weight", hiddenSize));
		
	}
	
	private void write( double[][] w, String fname) {
		File f = new File(fname);
		try(FileOutputStream fos = new FileOutputStream(f);
			PrintStream out = new PrintStream(fos)) {
			f.createNewFile();
			for (int ir = 0; ir < w.length; ir++) {
				for (int ic = 0; ic < w[ir].length; ic++) {
					out.printf(String.format("%.8f\n", w[ir][ic]) );
				}
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

}
