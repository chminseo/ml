package browser;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class InputPanel extends JPanel {

	final private static int X=0, Y=1, W=2, H=3 ;
	
	private List<List<double[]>> traces = new ArrayList<>();
	List<double [] > tracing = new ArrayList<>();
	
	private float thickness = 10.0f;
	private Stroke stroke = new BasicStroke(thickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	public InputPanel() {
		setPreferredSize(new Dimension(200, 200));
		
		Pencil p = new Pencil();
		this.addMouseListener(p);
		this.addMouseMotionListener(p);
	}
	
	public void appendTrace(ArrayList<double[]> t) {
		traces.add(t);
	}
	public void clearTrace() {
		traces.clear();
		repaint();
	}
	@Override
	protected void paintComponent(Graphics g1) {
		super.paintComponent(g1);
		int W = getWidth();
		int H = getHeight();
		int margin = 10;
		
		double [] box = pencilArea();
		
		Graphics2D g = (Graphics2D) g1;
		drawBg ( g , W, H, margin );
		drawWritingArea( g, W, H );
		drawTrace ( g, box );
	}

	private void drawTrace(Graphics2D g, double[] box) {
		
		
		g.setColor(Color.BLACK);
		g.setStroke(stroke);
		for ( List<double[]> trace : traces) {
			
			int [] x = new int[trace.size()];
			int [] y = new int[trace.size()];
			
			IntStream.range(0, trace.size()).forEach(idx -> {
				double [] p = trace.get(idx);
				x[idx] = (int) (box[X] + p[X]);
				y[idx] = (int) (box[Y] + p[Y]);
			});
			
			g.drawPolyline(x, y, x.length);
		}
		
		int [] x = new int[tracing.size()];
		int [] y = new int[tracing.size()];
		
		IntStream.range(0, tracing.size()).forEach(idx -> {
			double [] p = tracing.get(idx);
			x[idx] = (int) (box[X] + p[X]);
			y[idx] = (int) (box[Y] + p[Y]);
		});
		
		g.drawPolyline(x, y, x.length);
		
		
	}
	
	

	private void drawWritingArea(Graphics2D g, double W, double H) {
		double cx = W/ 2;
		double cy = H / 2;
		
		double left = cx - 100;
		double top  = cy - 100;
		
		g.setColor(Color.BLACK);
		g.drawRect((int)left, (int)top, 200, 200);
		
	}

	private void drawBg(Graphics2D g, int W, int H, int margin) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, W, H);
	}
	
	double [] pencilArea() {
		int W = getWidth();
		int H = getHeight();
		double cx = W / 2;
		double cy = H / 2;
		
		return new double[]{cx - 100, cy - 100, 200, 200 };
	}
	
	class Pencil extends MouseAdapter {
		boolean shouldClear = false;
		
		
		@Override
		public void mouseExited(MouseEvent e) {
			shouldClear = true;
		}
		@Override
		public void mousePressed(MouseEvent e) {
			if ( shouldClear ) {
				clearTrace();
				shouldClear = false;
			}
		}
		@Override
		public void mouseEntered(MouseEvent e) {
			InputPanel panel = (InputPanel) e.getSource();
			panel.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		}
		@Override
		public void mouseDragged(MouseEvent e) {
			int px = e.getX();
			int py = e.getY();
			
			double [] box = pencilArea();
			
			tracing.add(new double[]{px - box[X], py - box[Y]});
			InputPanel panel = (InputPanel) e.getSource();
			panel.repaint();
		}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			appendTrace ( new ArrayList<>(tracing) );
			tracing.clear();
		}
	}

	public BufferedImage getImage(int w, int h) {
		BufferedImage img = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) img.getGraphics();
		
		drawBg(g, 200, 200, 0);
		paintBoxArea ( g );
		g.dispose();
		
		BufferedImage result = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
		g = result.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(img, 0, 0, w, h, null);
		
		return result;
	}
	
	public int [] getImageBytes (int w, int h) {
		BufferedImage img = getImage(w, h);
		int [] data = new int[w * h ];
		
		int i = 0;
		for ( int iy = 0; iy < h ; iy ++ ) {
			for ( int ix = 0; ix < w ; ix ++ ) {
				data[i++] = img.getRGB(ix, iy) ;
			}
		}
		
		return data;
		
	}

	private void paintBoxArea(Graphics2D g) {
		double [] box = new double[]{0, 0, 200, 200};
		drawTrace(g, box);
	}



}
