package browser;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import github.yeori.mnist.MnistUtil;
import github.yeori.mnist.db.MnistDb;
import github.yeori.mnist.util.Util;
import perceptron.MLP;
import perceptron.MLPBuilder;
import perceptron.fn.BinarySigmoid;
import perceptron.fn.BipolarSigmoid;
import perceptron.fn.MathFunction;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.event.ActionEvent;

public class TestFrame extends JFrame {

	private JPanel contentPane;
	public static MLP mlp ;
	private ProbPanel probPanel;
	private InputPanel pencilPanel;
	private AnswerPanel answerPanel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
//		InputStream hwStream = TestFrame.class.getResourceAsStream("20160930_153237-hw-L350-0.20rate-30epoch.weights");
//		InputStream owStream = TestFrame.class.getResourceAsStream("20160930_153237-ow-L350-0.20rate-30epoch.weights");
//		MathFunction fn = new BinarySigmoid(0.2);
		InputStream hwStream = TestFrame.class.getResourceAsStream("20161002_033523-hw-L350-0.30rate-30epoch-idx.shuffle-bns.2-bns.2-wXV.weights");
		InputStream owStream = TestFrame.class.getResourceAsStream("20161002_033523-ow-L350-0.30rate-30epoch-idx.shuffle-bns.2-bns.2-wXV.weights");
		MathFunction fnHdn = new BipolarSigmoid(0.2);
		MathFunction fnOut = new BinarySigmoid(0.2);
		MLPBuilder builder = MLPBuilder.newBuilder();
		mlp = builder.feature(28*28)
				     .hiddenLayer(350,fnHdn)
				     .outputLayer(10, fnOut)
				     .setHiddenWeight(weights(hwStream))
				     .setOutputWeight(weights(owStream))
				     .learingRate(0.3)
		             .make();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestFrame frame = new TestFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static double[] weights(InputStream in) {
		if ( in == null ) {
			throw new RuntimeException("null weight stream");
		}
		Scanner sc = new Scanner(in);
		List<Double> val = new ArrayList<>();
		while ( sc.hasNext() ) {
			val.add( sc.nextDouble());
		}
		sc.close();
		return val.stream().mapToDouble(v->v.doubleValue()).toArray();
	}

	/**
	 * Create the frame.
	 */
	public TestFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		probPanel = new ProbPanel();
		contentPane.add(probPanel, BorderLayout.EAST);
		
		pencilPanel = new InputPanel();
		contentPane.add(pencilPanel, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		JButton btnGuess = new JButton("GUESS");
		btnGuess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				int [] pixels = pencilPanel.getImageBytes(28, 28);
				byte [] img = asMnistBytes ( pixels );
				System.out.println(Arrays.toString(img));
				double [] input = new double[img.length];
				conv(img, input);
				
				double [] output = mlp.training(input);
				for (int i = 0; i < output.length; i++) {
					System.out.printf("%d: %.4f\n", i, output[i]);
				}
				probPanel.render(output);
				
				processResult ( answerPanel.getAnswerNum(true), output, img );
				/* for display */
				for ( int ir = 0; ir < 28 ; ir ++) {
					for ( int ic = 0 ; ic < 28 ; ic ++ ) {
						System.out.print( String.format("%3s", Integer.toHexString(Util.b2i(img[28*ir + ic])).toUpperCase() ) );
					}
					System.out.println();
				}
				
			}
		});
		panel.add(btnGuess);
		
		answerPanel = new AnswerPanel();
		contentPane.add(answerPanel, BorderLayout.NORTH);
	}

	protected void processResult(int answer, double[] probs, byte[] rawdata) {
		int output = IntStream.range(0, probs.length)
				              .reduce( (i, j) -> probs[i] >= probs[j] ? i : j)
				              .getAsInt();
	}

	protected byte[] asMnistBytes(int[] pixels) {
		byte [] img = new byte[pixels.length];
		for (int i = 0; i < img.length; i++) {
			if ( pixels[i] == -1 ) {
				// FF FF FF FF : white;
				img[i] = 0;
			} else {
				img[i] = (byte)255;
			}
		}
		return img;
	}

	private double[] conv( byte[] from , double[] to) {
		for (int i = 0; i < to.length; i++) {
			to[i] = (Util.b2i(from[i]) - 128) / 128.0;
		}
		return to;
	}
	
}
