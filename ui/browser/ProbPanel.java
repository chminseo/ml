package browser;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class ProbPanel extends JPanel {

	private static double[] ZERO_PROB = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	private double [] probs = ZERO_PROB;
	private int cellMargin = 1;
	
	private Font numFont = new Font("Curior New", Font.BOLD, 14);
	private Font probFont = new Font("Curior New", Font.PLAIN, 12);
	private FontMetrics fm = getFontMetrics(numFont);
	/**
	 * Create the panel.
	 */
	public ProbPanel() {
//		render(new double[]{0.05, 0.192, 0.321, 0.033, 0.0122, 0.455, 0.7, 0.8, 0.9, 1.0});
		setPreferredSize(new Dimension(200, 20*10));
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		double W = getWidth();
		double H = getHeight();
		
		drawBG ( g, Color.WHITE, W, H);
		double dH = Math.max( 20.0, H / 10 ) ;
		
		Graphics2D g2d = (Graphics2D) g;
		for ( int i = 0 ; i < probs.length ; i ++ ) {
			g2d.translate(0, i*dH );
			drawProb( g2d, String.valueOf(i), probs[i], W, dH, cellMargin );
			g2d.translate(0, -i*dH );
		}
	}
	
	private void drawBG(Graphics g, Color bg, double W, double H) {
		g.setColor(bg);
		g.fillRect(0, 0, (int)W, (int)H);
	}

	private void drawProb(Graphics2D g, String num, double prob, double W, double H, int margin) {
		int top = margin;
		int left = margin ;
		double width = W - 2*margin;
		double height = H - 2*margin;
		
		drawProb (g, prob, top, left, width, height);
		drawFont (g, num, top, left+4, height, numFont);
		drawFont (g, String.valueOf(prob), top, left + height, height, probFont );
//		System.out.println(numLabel.isShowing());
	}

	private void drawProb(Graphics2D g, double prob, int top, int left, double W, double H) {
		g.setColor(Color.MAGENTA);
		g.fillRect(top, left, (int)(W*prob), (int)H);
		g.setColor(Color.BLACK);
		g.drawRect(top, left, (int)(W-1), (int)(H-1) );
	}

	private void drawFont(Graphics2D g, String num, double top, double left, double size, Font font) {
		
		FontMetrics fm = getFontMetrics(font);
		int fontWidth  = fm.stringWidth(num);
		int fontHeight = fm.getHeight();
		g.setFont(font);
		g.setColor(Color.BLACK);
		g.drawString(num, (int)(left), (int)((size + fontHeight)/2) );
		
	}

	public void render(double [] probs) {
		this.probs = probs;
		repaint();
	}

}
