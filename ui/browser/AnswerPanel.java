package browser;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.SwingConstants;

public class AnswerPanel extends JPanel {

	private Font inactive = new Font("Courier New", Font.PLAIN, 18);
	
	private Font active = new Font("Courier New", Font.BOLD, 18);
	
	private JLabel [] numLables = new JLabel[10];
	private int answerIndex = 0;
	/**
	 * Create the panel.
	 */
	public AnswerPanel() {
		setLayout(new GridLayout(0, 10, 0, 0));
		
		JLabel n0 = new JLabel("0");
		n0.setHorizontalAlignment(SwingConstants.CENTER);
		add(n0);
		numLables[0] = n0;
		
		JLabel n1 = new JLabel("1");
		n1.setHorizontalAlignment(SwingConstants.CENTER);
		add(n1);
		numLables[1] = n1;
		
		JLabel n2 = new JLabel("2");
		n2.setHorizontalAlignment(SwingConstants.CENTER);
		add(n2);
		numLables[2] = n2;
		
		JLabel n3 = new JLabel("3");
		n3.setHorizontalAlignment(SwingConstants.CENTER);
		add(n3);
		numLables[3] = n3;
		
		JLabel n4 = new JLabel("4");
		n4.setHorizontalAlignment(SwingConstants.CENTER);
		add(n4);
		numLables[4] = n4;
		
		JLabel n5 = new JLabel("5");
		n5.setHorizontalAlignment(SwingConstants.CENTER);
		add(n5);
		numLables[5] = n5;
		
		JLabel n6 = new JLabel("6");
		n6.setHorizontalAlignment(SwingConstants.CENTER);
		add(n6);
		numLables[6] = n6;
		
		JLabel n7 = new JLabel("7");
		n7.setHorizontalAlignment(SwingConstants.CENTER);
		add(n7);
		numLables[7] = n7;
		
		JLabel n8 = new JLabel("8");
		n8.setHorizontalAlignment(SwingConstants.CENTER);
		add(n8);
		numLables[8] = n8;
		
		JLabel n9 = new JLabel("9");
		n9.setHorizontalAlignment(SwingConstants.CENTER);
		add(n9);
		numLables[9] = n9;

		for (int i = 1; i < numLables.length; i++) {
			numLables[i].setFont(inactive);
		}
		numLables[answerIndex].setFont(active);
	}
	
	public int getAnswerNum(boolean proceed) {
		int num = answerIndex;
		if ( proceed) {
			answerIndex = (answerIndex + 1)% 10 ;
		}
		numLables[num].setFont(inactive);
		numLables[answerIndex].setFont(active);
		
		return num ;
	}

}
